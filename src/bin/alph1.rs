#[macro_use]
extern crate clap;

extern crate alph1;

use std::cmp::Ordering;
use std::collections::HashMap;
use std::fs;
use std::io;
use std::io::prelude::*;

use clap::Arg;

use alph1::*;

type Count = i64;
type Table = HashMap<String, Count>;
type Order = Fn(&(&String, &Count), &(&String, &Count)) -> Ordering;

fn main() {
    let args = app_from_crate!()
        .arg(
            Arg::with_name("alph")
                .short("a")
                .long("alph")
                .help("Alphabetize result"),
        )
        .arg(
            Arg::with_name("file")
                .short("f")
                .long("file")
                .takes_value(true)
                .default_value("TEXT-PCE-127.txt")
                .help("File to process"),
        )
        .get_matches();
    let cmp: Box<Order> = {
        if args.is_present("alph") {
            Box::new(|a, b| by_first(a, b))
        } else {
            Box::new(|a, b| by_second(a, b))
        }
    };
    let file = args.value_of("file").unwrap();
    let stdout = io::stdout();
    let mut stdout = stdout.lock();
    for i in 0..100 {
        let i = i + 1;
        writeln!(stdout, "# {:>22}: {:>10}", "Begin Iteration", i).unwrap();
        let table = {
            let mut table = Table::new();
            let mut f = io::BufReader::new(fs::File::open(file).unwrap());
            build_table(&mut table, &mut f).unwrap();
            table
        };
        report(&mut stdout, &table, |a, b| cmp(a, b)).unwrap();
        writeln!(stdout, "# {:>22}: {:>10}", "End Iteration", i).unwrap();
    }
}
