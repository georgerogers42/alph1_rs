#[macro_use]
extern crate lazy_static;
extern crate num;
extern crate regex;

use std::cmp::Ordering;
use std::collections::HashMap;
use std::fmt::Display;
use std::io;
use std::io::prelude::*;

use regex::Regex;

use num::traits::*;

pub fn capitalize(s: &str) -> String {
    let mut word = s.chars();
    let mut acc = word
        .next()
        .map(|c| c.to_uppercase().to_string())
        .unwrap_or_else(String::new);
    acc.extend(word);
    acc
}

pub fn create_table<V: Num + Send + Sync + Clone>(
    table: &mut HashMap<String, V>,
    r: &mut BufRead,
    pat: &Regex,
) -> io::Result<()> {
    for line in r.lines() {
        let line = line?;
        let words = pat.split(&line).filter(|w| *w != "").map(|w| capitalize(w));
        for word in words {
            let e = table.entry(word).or_insert_with(V::zero);
            *e = e.clone() + V::one();
        }
    }
    Ok(())
}

lazy_static! {
    static ref WORDS: Regex = Regex::new(r"\W+").unwrap();
}

pub fn build_table<V: Num + Send + Sync + Clone>(
    table: &mut HashMap<String, V>,
    r: &mut BufRead,
) -> io::Result<()> {
    create_table(table, r, &WORDS)
}

pub fn output<K: Display, V: Display, Pairs: IntoIterator<Item = (K, V)>>(
    w: &mut Write,
    pairs: Pairs,
) -> io::Result<()> {
    for (k, v) in pairs {
        writeln!(w, "{:>24}: {:>10}", k, v)?;
    }
    Ok(())
}

pub fn report<
    K: Display,
    V: Display,
    Tbl: IntoIterator<Item = (K, V)>,
    F: Fn(&(K, V), &(K, V)) -> Ordering,
>(
    w: &mut Write,
    table: Tbl,
    f: F,
) -> io::Result<()> {
    let mut pairs = table.into_iter().collect::<Vec<(K, V)>>();
    pairs.sort_by(f);
    output(w, pairs)
}

pub fn by_first<K: PartialOrd, V>(a: &(K, V), b: &(K, V)) -> Ordering {
    a.0.partial_cmp(&b.0).unwrap_or(Ordering::Greater)
}

pub fn by_second<K, V: PartialOrd>(a: &(K, V), b: &(K, V)) -> Ordering {
    a.1.partial_cmp(&b.1).unwrap_or(Ordering::Greater)
}
